# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


benches = Bench.create([
  {description: 'Painted Ladies', lat: 37.78, lng: -122.45},
  {description: 'Conservatory of Flowers', lat: 37.78, lng: -122.50},
  {description: 'InterContinental', lat: 37.76, lng: -122.44},
  {description: 'Alcatraz', lat: 37.80, lng: -122.46},
  {description: 'Cow Palace', lat: 37.72, lng: -122.49},
  ])
