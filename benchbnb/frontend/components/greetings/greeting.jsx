import React from 'react';
import {Link} from 'react-router-dom';

const Greeting = ({user, logout}) => {

    if(user){
      return (
        <div>
          <h3>Welcome! {user.username}</h3>
          <button onClick={() => logout()}>Log Out</button>
        </div>
      );
    } else {
      return(
        <div>
          <Link to="/signup">Sign Up</Link>
          <Link to="/login">Log In</Link>
        </div>
      );
    }

};

export default Greeting;
