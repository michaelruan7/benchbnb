import React from 'react';
import {withRouter, Link, Redirect} from 'react-router-dom';

class SessionForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.updateUsername = this.updateUsername.bind(this);
    this.updatePassword = this.updatePassword.bind(this);
  }
  handleSubmit(e) {
    e.preventDefault();
    const user = Object.assign({}, this.state);
    this.props.processForm(user);
  }

  updateUsername(e){
    this.setState({username: e.target.value});
  }
  updatePassword(e){
    this.setState({password: e.target.value});
  }

  render() {
    let typeForm = this.props.formType;
    if (typeForm === "login"){
      typeForm = "signup";
    } else {
      typeForm = "login";
    }
    // if (this.props.)
    let errors = "";
    if (this.props.errors.session){
      errors = this.props.errors.session.map((error,idx) => {
        return (<li key={idx}>{error}</li>);
    });
  }
    if (this.props.formType === undefined) {
      return <Redirect to="/" />;
    }

    return(
      <div>
        <form onSubmit={this.handleSubmit}>
          <h3>{this.props.formType}</h3>
          <label>Username
            <input type="text" onChange={this.updateUsername} value ={this.state.username}/>
          </label>

          <label>Password
            <input type="password" onChange={this.updatePassword} value={this.state.password} />
          </label>
          <button>Submit</button>
          <br />
          <Link to={`/${typeForm}`}> or {typeForm}</Link>
          <ul>
            {errors}
          </ul>
        </form>
      </div>
    );
  }
}

export default withRouter(SessionForm);
