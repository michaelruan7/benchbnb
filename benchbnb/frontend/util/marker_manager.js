export default class MarkerManager {
  constructor(map) {
    this.map = map;
    this.markers = {};
  }
  updateMarkers(benches) {
    debugger
    benches.forEach((bench) => {
      if (this.markers.keys === undefined){
        this.createMarkerFromBench(bench);
      } else if (!this.markers.keys.includes(bench.id)){
        this.createMarkerFromBench(bench);
      }
    });
    const objBenches = benches.reduce(function(acc, cur, i) {
      acc[i] = cur;
      return acc;
    }, {});
    this.markers.forEach((marker)=> {
      if (Object.keys(benches).indexOf(marker.id) === -1){
        this.removeMarker(marker);
      }
    });

  }

  removeMarker(marker){
    marker.setMap(null);
    delete this.marker[marker];
  }

  createMarkerFromBench(bench) {
    let marker = new google.maps.Marker({
      position: {lat: bench.lat,lng: bench.lng},
      map: this.map,
      animation: google.maps.Animation.DROP,
      title: bench.description
    });
    marker.setMap(this.map);
    this.markers[bench.id] = marker;
  }
}
