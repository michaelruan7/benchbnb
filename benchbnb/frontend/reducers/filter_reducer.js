import {UPDATE_BOUNDS} from '../actions/bench_actions';

const filterReducer = (oldState = {}, action) => {
  Object.freeze(oldState);
  switch (action.type) {
    case UPDATE_BOUNDS:
      return merge({}, oldState, {bounds: action.bounds} );
    default:
      return oldState;
  }
};

export default filterReducer;
