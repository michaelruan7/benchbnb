import {combineReducers} from 'redux';

import benchesReducer from './bench_reducer';

const entitiesReducer = combineReducers({
  benches: benchesReducer
});

export default entitiesReducer;
