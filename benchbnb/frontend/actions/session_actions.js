import * as APIUtil from '../util/session_api_util';
export const RECEIVE_CURRENT_USER = 'RECEIVE_CURRENT_USER';
export const RECEIVE_SESSION_ERRORS = 'RECEIVE_SESSION_ERRORS';


export const receiveCurrentUser = (currentUser) => {
  return ({
    type: RECEIVE_CURRENT_USER,
    user: currentUser
  });
};

export const receiveErrors = (errors) => {
  return ({
    type: RECEIVE_SESSION_ERRORS,
    errors
  });
};

export const signup = (user) => {
  return (
    (dispatch) => {
      return (
        APIUtil.signup(user)
        .then(currentUser => dispatch(receiveCurrentUser(currentUser)),
        (errors) => dispatch(receiveErrors(errors.responseJSON.errors)))
        // .catch((errors) => console.log(errors))
      );
    }
  );
};

// errors => dispatch(receiveErrors(errors))

export const login = (user) => (dispatch) => {
  return (
    APIUtil.login(user)
    .then(currentUser => dispatch(receiveCurrentUser(currentUser)),
    (errors) => dispatch(receiveErrors(errors.responseJSON.errors)))
  );
};

export const logout = () => (dispatch) => {
  return (
    APIUtil.logout()
    .then(() => dispatch(receiveCurrentUser(null)),
    (errors) => dispatch(receiveErrors(errors.responseJSON.errors)))
  );
};
