import * as APIUtil from "../util/bench_api_util";
import {fetchBenches} from './bench_actions.js';


export const UPDATE_BOUNDS = "UPDATE_BOUNDS";

export const updateBounds = bounds => {
  return {
    type: UPDATE_BOUNDS,
    bounds
  };
};

export function updateBound(bound, value) {
  return (dispatch, getState) => {
    dispatch(updateBounds(bound, value));
    return fetchBenches(getState().filters)(dispatch);
  };
}
