import React from 'react';
import ReactDOM from 'react-dom';
// import {signup, login, logout} from './util/session_api_util';
import {signup} from './actions/session_actions';
// import {fetchBenches} from './actions/bench_actions';
// import {fetchBenches} from './util/bench_api_util';
import configureStore from './store/store';
import Root from './components/root';

document.addEventListener('DOMContentLoaded', () => {
  let store;
  if (window.currentUser) {
    const preloadedState = { session: { currentUser: window.currentUser } };
    store = configureStore(preloadedState);
    delete window.currentUser;
  } else {
    store = configureStore();
  }
  window.getState = store.getState;
  window.dispatch = store.dispatch;
  // window.fetchBenches = fetchBenches;
  const root = document.getElementById('root');
  ReactDOM.render(<Root store={store} />, root);
});

window.signup = signup;
// window.login = login;
// window.logout = logout;
